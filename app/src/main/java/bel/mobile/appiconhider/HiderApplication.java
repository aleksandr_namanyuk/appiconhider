package bel.mobile.appiconhider;

import bel.mobile.appiconhider.di.AppComponent;
import bel.mobile.appiconhider.di.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

/**
 * Created by bel on 05/01/2018.
 */

public class HiderApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder()
                .application(this)
                .build();
        appComponent.inject(this);

        return appComponent;
    }
}
