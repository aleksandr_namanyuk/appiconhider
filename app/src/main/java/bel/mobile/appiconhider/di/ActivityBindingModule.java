package bel.mobile.appiconhider.di;

import bel.mobile.appiconhider.installedapplist.AppsActivity;
import bel.mobile.appiconhider.installedapplist.AppsActivityModule;
import bel.mobile.appiconhider.installedapplist.AppsActivityScope;
import bel.mobile.appiconhider.main.MainActivity;
import bel.mobile.appiconhider.main.MainActivityModule;
import bel.mobile.appiconhider.main.MainActivityScope;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by bel on 05/01/2018.
 */

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    @MainActivityScope
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = AppsActivityModule.class)
    @AppsActivityScope
    abstract AppsActivity bindAppsActivity();
}
