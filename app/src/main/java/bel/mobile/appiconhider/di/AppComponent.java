package bel.mobile.appiconhider.di;

import javax.inject.Singleton;

import bel.mobile.appiconhider.HiderApplication;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by bel on 05/01/2018.
 */
@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBindingModule.class})
public interface AppComponent extends AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder{
        @BindsInstance Builder application(HiderApplication application);
        AppComponent build();
    }

    void inject(HiderApplication application);

    @Override
    void inject(DaggerApplication instance);
}
