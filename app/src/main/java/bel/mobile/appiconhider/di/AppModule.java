package bel.mobile.appiconhider.di;

import android.content.Context;

import bel.mobile.appiconhider.HiderApplication;
import dagger.Binds;
import dagger.Module;

/**
 * Created by bel on 05/01/2018.
 */
@Module
public abstract class AppModule {

    @Binds
    abstract Context provideContext(HiderApplication application);
}
