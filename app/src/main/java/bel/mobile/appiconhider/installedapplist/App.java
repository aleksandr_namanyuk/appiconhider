package bel.mobile.appiconhider.installedapplist;

import android.graphics.drawable.Drawable;

/**
 * Created by bel on 05/01/2018.
 */

public class App {

    private String name;
    Drawable icon;

    public App(String name, Drawable icon) {
        this.name = name;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public Drawable getIcon() {
        return icon;
    }
}