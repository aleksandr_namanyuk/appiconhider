package bel.mobile.appiconhider.installedapplist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import bel.mobile.appiconhider.R;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by bel on 05/01/2018.
 */

public class AppsActivity extends DaggerAppCompatActivity implements AppsView{

    @Inject
    AppsActivityPresenter appsPresenter;

    AppsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_list);

        initRecyclerView();
        appsPresenter.loadInstalledApps();
    }

    private void initRecyclerView(){
        final RecyclerView rvApps = findViewById(R.id.rvApps);
        adapter = new AppsAdapter();
        rvApps.setAdapter(adapter);
        rvApps.setLayoutManager(new LinearLayoutManager(this));
        rvApps.setHasFixedSize(true);
        final RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        rvApps.addItemDecoration(itemDecoration);
    }

    @Override
    public void showApps(List<App> apps) {
        findViewById(R.id.progressBar).setVisibility(View.GONE);
        adapter.setAppList(apps);
    }

    @Override
    public void showLoading() {
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
    }

}
