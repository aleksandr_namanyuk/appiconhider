package bel.mobile.appiconhider.installedapplist;

import android.content.Context;

import bel.mobile.appiconhider.di.ActivityContext;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by bel on 05/01/2018.
 */

@Module
public abstract class AppsActivityModule {

    @Binds
    @AppsActivityScope
    @ActivityContext
    abstract Context provideContext(AppsActivity activity);

    @Binds
    @AppsActivityScope
    abstract AppsView provideAppsView(AppsActivity activity);

    @Provides
    @AppsActivityScope
    static AppsActivityPresenter AppsActivityPresenter(@ActivityContext Context context,AppsView appsView){
        return new AppsActivityPresenterImpl(context, appsView);
    }

}