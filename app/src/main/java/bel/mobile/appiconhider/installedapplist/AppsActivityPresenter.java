package bel.mobile.appiconhider.installedapplist;

/**
 * Created by lunni on 05/01/2018.
 */

public interface AppsActivityPresenter {

    void initAdapter();
    void loadInstalledApps();

}
