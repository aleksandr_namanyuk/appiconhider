package bel.mobile.appiconhider.installedapplist;


import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Log;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by bel on 05/01/2018.
 */

public class AppsActivityPresenterImpl implements AppsActivityPresenter {

    private static final String TAG = "AppsPresenterImpl";

    private AppsView appsView;
    private Context context;

    @Inject
    AppsActivityPresenterImpl(Context context, AppsView appsView) {
        this.context = context;
        this.appsView = appsView;
    }

    @Override
    public void initAdapter() {}

    @Override
    public void loadInstalledApps() {
        Observable.just(context.getPackageManager().getInstalledPackages(0))
                .doOnNext(e -> appsView.showLoading())
                .filter(packageInfos -> packageInfos.size() > 0)
                .flatMapIterable(packageInfos -> packageInfos)
                .filter(packageInfo -> (packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0)
                .map(p -> new App(p.applicationInfo.loadLabel(context.getPackageManager()).toString(),
                        p.applicationInfo.loadIcon(context.getPackageManager())))
                .toSortedList((p1,p2) -> p1.getName().compareToIgnoreCase(p2.getName()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(apps -> appsView.showApps(apps),
                           error -> Log.d(TAG, "-> error " + error.toString()),
                           () -> Log.d(TAG, "-> complete "));
    }

}
