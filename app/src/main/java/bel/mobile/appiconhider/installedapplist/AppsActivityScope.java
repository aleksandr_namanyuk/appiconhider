package bel.mobile.appiconhider.installedapplist;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by bel on 05/01/2018.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AppsActivityScope {
}
