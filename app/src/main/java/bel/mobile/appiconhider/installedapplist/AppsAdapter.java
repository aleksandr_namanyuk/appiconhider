package bel.mobile.appiconhider.installedapplist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.appiconhider.R;

/**
 * Created by bel on 05/01/2018.
 */

public class AppsAdapter extends RecyclerView.Adapter<AppsAdapter.ViewHolder> {

    List<App> apps;

    public AppsAdapter() {
        this.apps = new ArrayList<>();
    }

    public void setAppList(List<App> apps){
        this.apps = apps;
        notifyDataSetChanged();
    }

    @Override
    public AppsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Context context = parent.getContext();
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.installed_app_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AppsAdapter.ViewHolder holder, int position) {
        App app = apps.get(position);

        holder.imageIcon.setImageDrawable(app.getIcon());
        holder.textName.setText(app.getName());
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageIcon;
        TextView textName;

        ViewHolder(View itemView) {
            super(itemView);
            imageIcon = itemView.findViewById(R.id.list_app_icon);
            textName = itemView.findViewById(R.id.list_app_name);
        }
    }

}
