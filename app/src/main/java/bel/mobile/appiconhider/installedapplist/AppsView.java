package bel.mobile.appiconhider.installedapplist;

import java.util.List;

/**
 * Created by lunni on 05/01/2018.
 */

public interface AppsView {

    void showApps(List<App> apps);
    void showLoading();
}
