package bel.mobile.appiconhider.main;

import android.content.Context;

import bel.mobile.appiconhider.di.ActivityContext;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by bel on 05/01/2018.
 */
@Module
public abstract class MainActivityModule {

    @Binds
    @MainActivityScope
    @ActivityContext
    abstract Context provideContext(MainActivity activity);

    @Binds
    @MainActivityScope
    abstract MainView provideMainView(MainActivity activity);

    @Provides
    @MainActivityScope
    static MainActivityPresenter provideMainPresenter(@ActivityContext Context context, MainView mainView){
        return new MainActivityPresenterImpl(context, mainView);
    }
}
