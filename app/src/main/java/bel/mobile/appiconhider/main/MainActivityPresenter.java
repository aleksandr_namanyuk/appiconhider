package bel.mobile.appiconhider.main;

/**
 * Created by bel on 05/01/2018.
 */

public interface MainActivityPresenter {

    void showHiddenApps();
    void showNotHiddenApps();

}
