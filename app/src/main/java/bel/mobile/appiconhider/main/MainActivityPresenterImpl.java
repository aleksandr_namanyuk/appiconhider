package bel.mobile.appiconhider.main;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

/**
 * Created by bel on 05/01/2018.
 */

public class MainActivityPresenterImpl implements MainActivityPresenter {

    private static final String TAG = "MainPresenterImpl";

    private Context context;
    private MainView mainView;

    @Inject
    MainActivityPresenterImpl(Context context, MainView mainView) {
        this.context = context;
        this.mainView = mainView;
    }

    @Override
    public void showHiddenApps() {
        Log.d(TAG, "showHiddenApps() ");
    }

    @Override
    public void showNotHiddenApps() {
        Log.d(TAG, "showNotHiddenApps() ");
        mainView.startAppsActivity();
    }
}
