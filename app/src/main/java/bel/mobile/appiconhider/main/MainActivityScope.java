package bel.mobile.appiconhider.main;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by lunni on 05/01/2018.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MainActivityScope {
}
